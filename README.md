# Uber Coding Challenge - SF Film Location #

### Provides search on top of sf film location dataset [Film Locations](https://data.sfgov.org/Culture-and-Recreation/Film-Locations-in-San-Francisco/yitu-d5am) ###

* **Quick summary**

    A service implementation which can fetch data from sf film location data set and show this on google map with location markers. It provides auto suggest feature also by exposing necessary api.   

* **Version**

     v1

* **Demo**

    [App](http://45.32.234.170:8080/)

    [Api Docs](http://45.32.234.170:8080/api)   

### Steps to set up ###

* **Configuration**

    *application.properties*: 

     file contains several properties which can be tuned like database, elasticsearch, caching, scheduling etc.

* **Dependencies**

    Java 8

    Maven 3.2

    Elasticsearch 1.5.2

    Mysql 5.5

* **How to run tests**

    run following command from <project root>/sf-api: 

    ```
    mvn clean install
    ```

    It runs integration tests with in memory hsqldb and in memory elastic search

* **Deployment instructions**
    
    Execute below command

    ```
    mvn clean install
    ```

    It generates sf-api-0.0.1-SNAPSHOT.war in target folder

    It can be deployed on tomcat or can be executed like java -jar sf-api-0.0.1-SNAPSHOT.war which uses embedded tomcat


* **Architecture**
 
    **Service has following main components**

    **FilmLocationDataUpdateTask.update:** A cron job which periodically checks if new data set is available. If present, downloads it and stores it in database and indexes in elasticsearch. It first marks the new data set as staged, once done then atomically makes it active and old one inactive. By default runs every Friday at 23:00 Hrs

    **FilmLocationDataUpdateTask.cleanup:** Deletes all old data sets from database and elasticsearch periodically. By default runs every day at 05:00 Hrs
   
    **FilmLocationDataUpdateTask.updateFilmLocationForGeoLocation:** Updates downloaded film locations asynchronously. 

    **Three levels of cache:** Application uses 3 levels of caching.

     *user-cache*: 

      server side cache which caches response for searched film locations by title

     *meta-cache*: 

      server side cache which store meta objects like geo locations, active datasets. As geolocations are constants, no need to download them every time, instead persist them once downloaded. Active dataset id is also cached and gets updated when new data set arrives.

     *browser cache*: 

      for every successful search request, result is cached in browser cache by sending appropriate caching headers. As data does not change very often a default 7 days cache policy is used.

    **Indexing:** Film titles with dataset id are indexed in elastic search. Using completion suggestor, auto complete functionality is provided by suggest api.

    **Cors**: apis are cors enabled for all domains.

    **api working**: There are 3 apis as described in [Api Docs](http://45.32.234.170:8080/api)

    ```   
    /suggest
    ```

    for every keyword it tries to find matching titles from elastic search and returns the result 

    ```
    /all
    ```

    returns all film locations having geo location information available

    ```
    /search
    ```

    given a title, it tries to find all film locations associated with it in cache, if not then   in database. It then checks if all of them have geo locations, if not it fetches the geo coordinates for these locations from geo code api in parallel. Once done it launches a async task **FilmLocationDataUpdateTask.updateFilmLocationForGeoLocation** to update these geo locations and respective film locations in database & cache. After launching the task api caches the response and returns response. Hence **first request for a film title is expensive** after that it is very fast. All successful searches (with 'OK' status) are cached. A LRU cache implementation from google guava is used.
  
* **My contribution**

    All code in this repository in sf-api folder is written by me. I have used many frameworks
 here. Below is my experience in these technologies.

    spring core: 3-4 years

    spring boot: new

    hibernate: 1-2 years

    mysql: 4 year

    elastic search: 6 months

    guava cache: 6 months

    Java 8: 6 months (but have around 5 years of experience in Java 5/6)