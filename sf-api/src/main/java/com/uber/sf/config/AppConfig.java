package com.uber.sf.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import net.rossillo.spring.web.mvc.CacheControlHandlerInterceptor;

@Configuration
public class AppConfig extends WebMvcConfigurerAdapter {
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new CacheControlHandlerInterceptor());
		super.addInterceptors(registry);
	}
}
