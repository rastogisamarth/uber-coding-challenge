package com.uber.sf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.uber.sf.domain.GeoLocation;

public interface GeoLocationRepository extends JpaRepository<GeoLocation, Long> {
	public GeoLocation findByLocation(String location);
	public List<GeoLocation> findByLocationIn(List<String> location);
}
