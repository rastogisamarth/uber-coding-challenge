package com.uber.sf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.uber.sf.domain.FilmLocation;

public interface FilmLocationRepository extends JpaRepository<FilmLocation, Long> {
	public List<FilmLocation> findByFilmLocationDataSet_IdAndLocationsNotNullAndAndTitleIgnoreCase(long filmLocationDataSetId, String title);
	public List<FilmLocation> findByTitleNotNullAndGeoLocation_IdNotNullAndFilmLocationDataSet_Id(long filmLocationDataSetId);	
	public void deleteByFilmLocationDataSet_IdIn(List<Long> filmLocationDataSetIds);
}