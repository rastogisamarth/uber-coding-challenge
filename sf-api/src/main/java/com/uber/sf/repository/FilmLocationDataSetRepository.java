package com.uber.sf.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.uber.sf.domain.FilmLocationDataSet;
import com.uber.sf.domain.FilmLocationDataSet.State;

public interface FilmLocationDataSetRepository extends JpaRepository<FilmLocationDataSet, Long> {
	public List<FilmLocationDataSet> findByState(State state);
	public void deleteByState(State state);
}
