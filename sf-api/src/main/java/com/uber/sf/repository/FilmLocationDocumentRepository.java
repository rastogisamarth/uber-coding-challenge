package com.uber.sf.repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.elasticsearch.action.suggest.SuggestResponse;
import org.elasticsearch.index.query.TermsQueryBuilder;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;
import org.elasticsearch.search.suggest.completion.CompletionSuggestionBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.DeleteQuery;
import org.springframework.data.elasticsearch.core.query.IndexQuery;
import org.springframework.stereotype.Repository;

import com.uber.sf.domain.FilmLocationDocument;
import com.uber.sf.util.Constant;

@Repository
public class FilmLocationDocumentRepository {
	private static final Logger logger = LoggerFactory.getLogger(FilmLocationDocumentRepository.class);
	
	@Inject
	private ElasticsearchTemplate elasticsearchTemplate;

	@PostConstruct
	private void initialize() {
		if (!elasticsearchTemplate.indexExists(FilmLocationDocument.class)) {
			elasticsearchTemplate.createIndex(FilmLocationDocument.class);
			elasticsearchTemplate.refresh(FilmLocationDocument.class, true);			
			elasticsearchTemplate.putMapping(FilmLocationDocument.class);		
		}
	}

	public void save(Collection<FilmLocationDocument> entities) {
		if (Objects.nonNull(entities)) {
			List<IndexQuery> queries = entities.stream().map(FilmLocationDocument::toIndexQuery).collect(Collectors.toList());
			elasticsearchTemplate.bulkIndex(queries);
			elasticsearchTemplate.refresh(FilmLocationDocument.class, true);
		}
	}
	
	// datasetid is not being used currently as needed context completion
	// it is not supported in spring data elasticsearch, hence dropped the
	// idea to implement
	public List<String> findByFilmTitleStartingWith(long dataSetId, String text) {
		logger.debug("finding autosuggestion for: {}", text);
		CompletionSuggestionBuilder builder = new CompletionSuggestionBuilder(Constant.SUGGEST)
				.text(text)
				.field(Constant.SUGGEST);
		SuggestResponse suggest = elasticsearchTemplate.suggest(builder, FilmLocationDocument.class);
		CompletionSuggestion suggestion = suggest.getSuggest().getSuggestion(Constant.SUGGEST);
		if (Objects.isNull(suggestion) || suggestion.getEntries().isEmpty()) {
			logger.debug("found 0 autosuggestion for: {}", text);
			return new ArrayList<>();
		}
		List<CompletionSuggestion.Entry.Option> options = suggestion.getEntries().get(0).getOptions();
		logger.debug("found {} autosuggestion for: {}", options.size(), text);
		return options.stream().map(e -> e.getText().string()).collect(Collectors.toList());		
	}

	public void deleteByFilmLocationDataSetIdIn(List<Long> dataSetIds) {
		DeleteQuery deleteQuery = new DeleteQuery();
		deleteQuery.setQuery(new TermsQueryBuilder(Constant.DATASET_ID, dataSetIds));
		elasticsearchTemplate.delete(deleteQuery, FilmLocationDocument.class);
		elasticsearchTemplate.refresh(FilmLocationDocument.class, false);
	}
}
