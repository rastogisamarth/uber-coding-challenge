package com.uber.sf.task;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.google.common.base.Preconditions;
import com.socrata.api.HttpLowLevel;
import com.socrata.api.Soda2Consumer;
import com.socrata.builders.SoqlQueryBuilder;
import com.socrata.model.soql.SoqlQuery;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.ClientResponse.Status;
import com.sun.jersey.api.client.GenericType;
import com.uber.sf.domain.FilmLocation;
import com.uber.sf.domain.FilmLocationDataSet;
import com.uber.sf.domain.FilmLocationDataSet.State;
import com.uber.sf.domain.FilmLocationDocument;
import com.uber.sf.domain.GeoLocation;
import com.uber.sf.service.FilmLocationDataSetService;
import com.uber.sf.service.FilmLocationDocumentService;
import com.uber.sf.service.FilmLocationService;
import com.uber.sf.service.GeoLocationService;
import com.uber.sf.util.Constant;


@Component
public class FilmLocationDataUpdateTask {
	private static final Logger logger = LoggerFactory.getLogger(FilmLocationDataUpdateTask.class); 
	private static final int MAX_RECORDS = 50_000; // as using 2.0, not needed in 2.1
	@Inject
	private GeoLocationService geoLocationService;
	@Inject
	private FilmLocationService filmLocService;
	@Inject
	private FilmLocationDataSetService dataSetService;
	@Inject
	private FilmLocationDocumentService indexService;
	@Value("#{cacheManager.getCache('" + Constant.META_CACHE + "')}")
	private Cache metaCache;
	@Value("#{cacheManager.getCache('" + Constant.USER_CACHE + "')}")
	private Cache userCache;

	@Value("${socrata.api.endPointBaseUrl}")
	private String endPointBaseUrl;
	@Value("${socrata.api.resource}")
	private String apiResource;
	@Value("${socrata.api.username}")
	private String apiUsername;
	@Value("${socrata.api.password}")
	private String apiPassword;
	@Value("${socrata.api.appToken}")
	private String appToken;

	private Soda2Consumer consumer;

	@Value("${update.schedule.enable}")
	private boolean updateEnabled;
	@Value("${cleanup.schedule.enable}")
	private boolean cleanupEnabled;

	@PostConstruct
	protected void initialize() {
		Preconditions.checkNotNull(endPointBaseUrl, "api base url is null; configure this in properties");
		Preconditions.checkNotNull(apiUsername, "api username is null; configure this in properties");
		Preconditions.checkNotNull(apiPassword, "api password is null; configure this in properties");
		Preconditions.checkNotNull(apiResource, "api resource is null; configure this in properties");
		if (Objects.isNull(appToken)) {
			logger.warn("socrata app token is null, this may result in request throttling quite often.");
		}
		this.consumer = Soda2Consumer.newConsumer(endPointBaseUrl, apiUsername, apiPassword, appToken);
		this.consumer.getHttpLowLevel().setMaxRetries(10);
		this.consumer.getHttpLowLevel().setStatusCheckErrorRetries(10);
	}

	@Scheduled(cron = "${update.schedule}")
	public void update() {
		if (!updateEnabled) {
			return;
		}
		logger.info("update task: started");
		long start = System.currentTimeMillis();
		List<FilmLocationDataSet> activeDataSets = dataSetService.getByState(State.ACTIVE);
		FilmLocationDataSet activeDataSet = activeDataSets.size() > 0 ? activeDataSets.get(0) : null;
		Date latestDataSetTimeStamp = getLatestDataSetTimeStampFromSodaApi();
		if (Objects.isNull(latestDataSetTimeStamp)) {
			logger.info("update task: exiting due to api/network issue(needs investigation).");
			return;
		}
		if (Objects.nonNull(activeDataSet) && !isExpired(activeDataSet.getLastModified(), latestDataSetTimeStamp)) {
			logger.info("update task: exiting as data is already up to date.");
			return;
		}
		logger.debug("found active dataset: {}", activeDataSet);
		cleanUpByState(State.STAGE);
		ImmutablePair<Date, List<FilmLocation>> latestDateAndFilmLocations = getLatestFilmLocationFromSodaApi();
		if (Objects.isNull(latestDateAndFilmLocations)) {
			logger.info("update task: exiting due to api/network issue(needs investigation).");
			return;
		}
		
		FilmLocationDataSet newdataSet = stageFilmLocationDataSet(latestDateAndFilmLocations.getLeft(), latestDateAndFilmLocations.getRight());
		indexFilmLocationDocumentFor(newdataSet.getFilmLocations());
		dataSetService.updateActiveDataSet(activeDataSet, newdataSet);
		logger.debug("dataset {} is now active", newdataSet.getId());
		metaCache.evict(Constant.ACTIVE_DATASET_KEY);
		userCache.clear();
		long total = Duration.ofMillis(System.currentTimeMillis() - start).getSeconds();
		logger.info("update task finished in {} seconds.", total);
	}

	@Scheduled(cron = "${cleanup.schedule}")
	public void cleanUp() {
		if (!cleanupEnabled) {
			return;
		}
		logger.info("cleanup task: started");
		cleanUpByState(State.OLD);
		logger.info("cleanup task: finished");
	}
	
	@Async
	public void updateFilmLocationForGeoLocation(final List<FilmLocation> filmLocations) {
		filmLocations.stream().forEach(fl -> {
			GeoLocation geoLocation = geoLocationService.storeIfNotExist(fl.getGeoLocation());
			metaCache.putIfAbsent(geoLocation.getLocation(), geoLocation);
			fl.setGeoLocation(geoLocation);
			filmLocService.update(fl);
		});
		try {
			Thread.sleep(10_000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.debug("updateFilmLocationForGeoLocation: {} updated", filmLocations.size());
	}
	
	private void cleanUpByState(State state) {
		logger.debug("cleanup {}: started", state.name());
		List<FilmLocationDataSet> dataSets = dataSetService.getByState(state);
		List<Long> dataSetIds = dataSets.stream()
				.map(FilmLocationDataSet::getId)
				.collect(Collectors.toList());
		logger.debug("cleanup {}: deleting datasets {}", state.name(), dataSetIds);
		indexService.deleteByDataSetIds(dataSetIds);
		filmLocService.deleteByDataSetIds(dataSetIds);
		dataSetService.delete(dataSets);
		logger.debug("cleanup {}: removed {} datasets", state.name(), dataSets.size());
		logger.debug("cleanup {}: finished", state.name());
	}

	private void indexFilmLocationDocumentFor(List<FilmLocation> stagedFilmLocations) {
		List<FilmLocationDocument> documents = stagedFilmLocations.stream()
				.filter(fl -> Objects.nonNull(fl.getTitle()) && !fl.getTitle().isEmpty())
				.map(FilmLocationDocument::from)
				.collect(Collectors.toList());
		indexService.store(documents);
		logger.debug("indexed {} documents for dataset {}", documents.size());
	}

	private FilmLocationDataSet stageFilmLocationDataSet(Date timeStamp, List<FilmLocation> filmLocations) {
		FilmLocationDataSet dataSet = new FilmLocationDataSet();
		dataSet.setLastModified(timeStamp);
		dataSet.setState(State.STAGE);
		filmLocations.stream().forEach(fl -> {
			fl.setFilmLocationDataSet(dataSet);
			if (Objects.nonNull(fl.getLocations()) && !"".equals(fl.getLocations())) {
				fl.setGeoLocation(geoLocationService.getGeoLocationFor(fl.getLocations()));				
			}
		});
		dataSet.setFilmLocations(filmLocations);
		FilmLocationDataSet storedDataSet = dataSetService.store(dataSet);
		logger.debug("staged {} film locations for dataset {}", filmLocations.size(), storedDataSet.getId());
		return storedDataSet;
	}

	private Date convertToUTC(Date in) {
		LocalDateTime ldt = LocalDateTime.ofInstant(in.toInstant(), ZoneId.of("UTC"));
		Date out = Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
		return out;
	}

	private SoqlQuery getQuery(Integer limit) {
		return new SoqlQueryBuilder(SoqlQuery.SELECT_ALL).setLimit(limit).build();
	}

	private boolean isExpired(Date activeDataSetTimeStamp, Date latestDataSetTimeStamp) {
		return activeDataSetTimeStamp.before(latestDataSetTimeStamp);
	}

	private Date getLatestDataSetTimeStampFromSodaApi() {
		Optional<ClientResponse> apiResponse = callSodaApi(getQuery(1));
		if (apiResponse.isPresent()) {
			ClientResponse response = apiResponse.get();
			return convertToUTC(response.getLastModified());
		}
		return null;
	}

	private ImmutablePair<Date, List<FilmLocation>> getLatestFilmLocationFromSodaApi() {
		Optional<ClientResponse> apiResponse = callSodaApi(getQuery(MAX_RECORDS));
		if (apiResponse.isPresent()) {
			try {
				List<FilmLocation> filmLocations = apiResponse.get().getEntity(new GenericType<List<FilmLocation>>(){});
				Date lastModified = apiResponse.get().getLastModified();
				return ImmutablePair.of(convertToUTC(lastModified), filmLocations);
			} catch (Exception e) {
				logger.error("error occurred: ", e);
			}
		}
		return null;
	}
	
	private Optional<ClientResponse> callSodaApi(SoqlQuery query) {
		logger.debug("callSodaApi: started");
		Optional<ClientResponse> apiResponse = Optional.empty();
		try {
			ClientResponse response = consumer.query(apiResource, HttpLowLevel.JSON_TYPE, query);
			boolean isValid = false;
			if (Status.OK != response.getClientResponseStatus()) {
				logger.info("callSodaApi: api response status is {} but expected {}", Status.OK, response.getClientResponseStatus());
			} else if (Objects.isNull(response.getLastModified())) {
				logger.info("callSodaApi: api did not return last modified date");
			} else if (!response.hasEntity()) {
				logger.info("callSodaApi: api did not return any data");
			} else {
				isValid = true;
			}

			if (isValid) {
				apiResponse = Optional.of(response);				
			}
		} catch (Exception e) {
			logger.error("error occurred while calling soda api: ", e);
		}
		logger.debug("callSodaApi: finished");
		return apiResponse;
	}
}
