package com.uber.sf.web;

import java.util.Collections;
import java.util.Objects;

import javax.inject.Inject;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiAuthNone;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiQueryParam;
import org.jsondoc.core.annotation.ApiResponseObject;
import org.jsondoc.core.annotation.ApiVersion;
import org.jsondoc.core.pojo.ApiStage;
import org.jsondoc.core.pojo.ApiVisibility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Preconditions;
import com.uber.sf.service.FilmLocationDataSetService;
import com.uber.sf.service.FilmLocationService;
import com.uber.sf.util.Constant;

import net.rossillo.spring.web.mvc.CacheControl;
import net.rossillo.spring.web.mvc.CachePolicy;

@Api(
	name = "film location service", 
	description = "apis for searching films shooting locations in san francisco", 
	group = "entertainment", 
	visibility = ApiVisibility.PUBLIC, stage = ApiStage.RC)
@ApiVersion(since = "1.0")
@ApiAuthNone
@RestController
@CrossOrigin
@RequestMapping(value = Constant.SLASH_API_VERSION)
public class FilmLocationController {
	private static final Logger logger = LoggerFactory.getLogger(FilmLocationController.class);
	public static final String STATUS_OK = Constant.STATUS_OK;

	@Inject
	private FilmLocationService filmLocationService;
	@Inject
	private FilmLocationDataSetService filmLocationDataSetService;

	@RequestMapping(method=RequestMethod.GET, value="/all")
	@ApiResponseObject
	@ApiMethod(description = "returns all film locations having geolocation")
	public Response<FilmLocationVO> allfilmLocation() {
		Response<FilmLocationVO> response = filmLocationDataSetService.getActiveDataSetId()
				.map(id -> Response.okBuilder()
						.data(filmLocationService.getAllFilmLocation(id))
						.<FilmLocationVO>build())
				.orElse(Response.<FilmLocationVO>noDataResponse());
		logger.debug("{}", response);
		return response;
	}

	@RequestMapping(method=RequestMethod.GET, value="/suggest")
	@ApiResponseObject
	@ApiMethod(description = "returns all film titles starting with 'q'")
	public Response<String> suggestFilmTitle(@ApiQueryParam(description = "film titles starting with") @RequestParam("q") String startingWith) {
		Preconditions.checkArgument(Objects.nonNull(startingWith), "'q' can not be null");
		if ("".equals(startingWith)) {
			return Response.okBuilder().data(Collections.emptyList()).<String>build();
		}
		Response<String> response = filmLocationDataSetService.getActiveDataSetId()
				.map(id -> Response.okBuilder().data(filmLocationService.suggestFilmTitleStartingWith(id, startingWith))
						.<String>build())
				.orElse(Response.<String>noDataResponse());
		logger.debug("{}", response);
		return response;
	}

	@Cacheable(
		cacheNames = Constant.USER_CACHE, 
		unless = "#result.getStatus() != #root.targetClass.STATUS_OK",
		key = "#searchString")	
	@CacheControl(maxAge = Constant._7_DAYS, policy = {CachePolicy.PUBLIC})
	@RequestMapping(method=RequestMethod.GET, value="/search")
	@ApiMethod(description = "returns all film locations with title as 'q'")
	public Response<FilmLocationVO> searchFilmLocation(@ApiQueryParam(description = "title of film") @RequestParam("q") String searchString) {
		Preconditions.checkArgument(Objects.nonNull(searchString), "'q' can not be null");
		if ("".equals(searchString)) {
			return Response.okBuilder().data(Collections.emptyList()).<FilmLocationVO>build();
		}
		Response<FilmLocationVO> response = filmLocationDataSetService.getActiveDataSetId()
				.map(id -> Response.okBuilder()
						.data(filmLocationService.findFilmLocationByTitle(id, searchString))
						.<FilmLocationVO>build())
				.orElse(Response.<FilmLocationVO>noDataResponse());
		logger.debug("{}", response);
		return response;
	}
	
	@ExceptionHandler(value = {MissingServletRequestParameterException.class, IllegalArgumentException.class})
	public ResponseEntity<Response<String>> handleException(Exception e) {
		return new ResponseEntity<Response<String>>(Response.errorBuilder().message(e.getMessage()).<String>build(), HttpStatus.BAD_REQUEST);
	}
}
