package com.uber.sf.web;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {
	private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<Response<String>> defaultExceptionHandler(HttpServletRequest req, Exception e) {
		logger.error("Exception occurred for req: {}", req.getRequestURL().append("?" + req.getQueryString()), e);
		return new ResponseEntity<Response<String>>(
				Response.errorBuilder().message("Oops...don't worry we are on it.").<String>build(), 
				HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
