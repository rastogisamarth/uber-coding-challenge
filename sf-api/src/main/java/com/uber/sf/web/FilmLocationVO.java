package com.uber.sf.web;

import java.nio.charset.Charset;
import java.util.Base64;
import java.util.Objects;

import org.jsondoc.core.annotation.ApiObject;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.uber.sf.domain.FilmLocation;
import com.uber.sf.util.Constant;

@JsonInclude(Include.NON_NULL)
@ApiObject(name = "film location", description = "it contains film location details")
public class FilmLocationVO {
	private String id;
	private String actor1;
	private String actor2;
	private String actor3;
	private String director;
	private String distributor;
	private String funFacts;
	private String locations;
	private String productionCompany;
	private Integer releaseYear;
	private Integer smileAgainJennyLee;
	private String title;
	private String writer;
	private Double latitude;
	private Double longitude;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getActor1() {
		return actor1;
	}

	public void setActor1(String actor1) {
		this.actor1 = actor1;
	}

	public String getActor2() {
		return actor2;
	}

	public void setActor2(String actor2) {
		this.actor2 = actor2;
	}

	public String getActor3() {
		return actor3;
	}

	public void setActor3(String actor3) {
		this.actor3 = actor3;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getDistributor() {
		return distributor;
	}

	public void setDistributor(String distributor) {
		this.distributor = distributor;
	}

	public String getFunFacts() {
		return funFacts;
	}

	public void setFunFacts(String funFacts) {
		this.funFacts = funFacts;
	}

	public String getLocations() {
		return locations;
	}

	public void setLocations(String locations) {
		this.locations = locations;
	}

	public String getProductionCompany() {
		return productionCompany;
	}

	public void setProductionCompany(String productionCompany) {
		this.productionCompany = productionCompany;
	}

	public Integer getReleaseYear() {
		return releaseYear;
	}

	public void setReleaseYear(Integer releaseYear) {
		this.releaseYear = releaseYear;
	}

	public Integer getSmileAgainJennyLee() {
		return smileAgainJennyLee;
	}

	public void setSmileAgainJennyLee(Integer smileAgainJennyLee) {
		this.smileAgainJennyLee = smileAgainJennyLee;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public static FilmLocationVO from(FilmLocation filmLocation) {
		FilmLocationVO filmLocationVO = new FilmLocationVO();
		String encodedId = Base64.getEncoder()
				.encodeToString(String.valueOf(filmLocation.getId()).getBytes(Charset.forName(Constant.UTF_8)));
		filmLocationVO.setId(encodedId);
		filmLocationVO.setActor1(filmLocation.getActor_1());
		filmLocationVO.setActor2(filmLocation.getActor_2());
		filmLocationVO.setActor3(filmLocation.getActor_3());
		filmLocationVO.setDirector(filmLocation.getDirector());
		filmLocationVO.setDistributor(filmLocation.getDistributor());
		filmLocationVO.setFunFacts(filmLocation.getFun_facts());
		filmLocationVO.setLocations(filmLocation.getLocations());
		filmLocationVO.setProductionCompany(filmLocation.getProduction_company());
		filmLocationVO.setReleaseYear(filmLocation.getRelease_year());
		filmLocationVO.setSmileAgainJennyLee(filmLocation.getSmile_again_jenny_lee());
		filmLocationVO.setTitle(filmLocation.getTitle());
		filmLocationVO.setWriter(filmLocation.getWriter());
		if (Objects.nonNull(filmLocation.getGeoLocation())) {
			filmLocationVO.setLatitude(filmLocation.getGeoLocation().getLatitude());
			filmLocationVO.setLongitude(filmLocation.getGeoLocation().getLongitude());
		}
		return filmLocationVO;
	}
}
