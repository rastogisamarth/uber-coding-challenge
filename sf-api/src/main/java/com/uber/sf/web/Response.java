package com.uber.sf.web;

import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.uber.sf.util.Constant;

@JsonInclude(Include.NON_NULL)
@ApiObject
public class Response<T> {
	@ApiObjectField(description = "'OK' for valid response, 'NO_DATA' when no data available, 'ERROR' when error occurs")
	private final String status;
	@ApiObjectField(description = "message from server")
	private final String message;
	@ApiObjectField(description = "list of film locations or string")
	private final Object data;

	private Response(String status, String message, Object data) {
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public String getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

	public Object getData() {
		return data;
	}

	public static Builder newBuilder(String status) {
		return new Builder(status);
	}

	public static Builder okBuilder() {
		return newBuilder(Constant.STATUS_OK);
	}

	public static Builder errorBuilder() {
		return newBuilder(Constant.STATUS_ERROR);
	}

	public static <T> Response<T> noDataResponse() {
		return newBuilder(Constant.STATUS_NO_DATA).message("No data available at the moment").<T>build();
	}

	public static class Builder {
		private final String status;
		private String message;
		private Object data;

		public Builder(String status) {
			this.status = status;
		}

		public Builder message(String message) {
			this.message = message;
			return this;
		}

		public Builder data(Object data) {
			this.data = data;
			return this;
		}

		public <T> Response<T> build() {
			return new Response<T>(status, message, data);
		}
	}

	@Override
	public String toString() {
		return "Response [status=" + status + ", message=" + message + ", data=" + data + "]";
	}
}
