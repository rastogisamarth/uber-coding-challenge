package com.uber.sf.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class FilmLocation {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(length = 100)
	private String actor_1;
	@Column(length = 100)
	private String actor_2;
	@Column(length = 100)
	private String actor_3;
	@Column(length = 100)
	private String director;
	@Column(length = 100)
	private String distributor;
	@Column(length = 500)
	private String fun_facts;
	@Column(length = 200)
	private String locations;
	@Column(length = 100)
	private String production_company;
	private Integer release_year;
	private Integer smile_again_jenny_lee;
	@Column(length = 100)
	private String title;
	@Column(length = 100)
	private String writer;
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable = false)
	private FilmLocationDataSet filmLocationDataSet;
	@JsonIgnore
	@ManyToOne
	private GeoLocation geoLocation;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getActor_1() {
		return actor_1;
	}

	public void setActor_1(String actor_1) {
		this.actor_1 = actor_1;
	}

	public String getActor_2() {
		return actor_2;
	}

	public void setActor_2(String actor_2) {
		this.actor_2 = actor_2;
	}

	public String getActor_3() {
		return actor_3;
	}

	public void setActor_3(String actor_3) {
		this.actor_3 = actor_3;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getDistributor() {
		return distributor;
	}

	public void setDistributor(String distributor) {
		this.distributor = distributor;
	}

	public String getFun_facts() {
		return fun_facts;
	}

	public void setFun_facts(String fun_facts) {
		this.fun_facts = fun_facts;
	}

	public String getLocations() {
		return locations;
	}

	public void setLocations(String locations) {
		this.locations = locations;
	}

	public String getProduction_company() {
		return production_company;
	}

	public void setProduction_company(String production_company) {
		this.production_company = production_company;
	}

	public Integer getRelease_year() {
		return release_year;
	}

	public void setRelease_year(Integer release_year) {
		this.release_year = release_year;
	}

	public Integer getSmile_again_jenny_lee() {
		return smile_again_jenny_lee;
	}

	public void setSmile_again_jenny_lee(Integer smile_again_jenny_lee) {
		this.smile_again_jenny_lee = smile_again_jenny_lee;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public FilmLocationDataSet getFilmLocationDataSet() {
		return filmLocationDataSet;
	}

	public void setFilmLocationDataSet(FilmLocationDataSet filmLocationDataSet) {
		this.filmLocationDataSet = filmLocationDataSet;
	}

	public GeoLocation getGeoLocation() {
		return geoLocation;
	}

	public void setGeoLocation(GeoLocation geoLocation) {
		this.geoLocation = geoLocation;
	}

	@Override
	public String toString() {
		return "FilmLocation [id=" + id + ", actor_1=" + actor_1 + ", actor_2=" + actor_2 + ", actor_3=" + actor_3
				+ ", director=" + director + ", distributor=" + distributor + ", fun_facts=" + fun_facts
				+ ", locations=" + locations + ", production_company=" + production_company + ", release_year="
				+ release_year + ", smile_again_jenny_lee=" + smile_again_jenny_lee + ", title=" + title + ", writer="
				+ writer + ", filmLocationDataSetInfo=" + filmLocationDataSet + ", geoLocation=" + geoLocation
				+ "]";
	}
}
