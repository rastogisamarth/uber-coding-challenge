package com.uber.sf.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.CompletionField;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.core.completion.Completion;
import org.springframework.data.elasticsearch.core.query.IndexQuery;

import com.uber.sf.util.Constant;

@Document(indexName = Constant.FILM_LOCATION, type = Constant.FILM_LOCATION, shards = 1, replicas = 0)
public class FilmLocationDocument {
	@Id
	private String id;
	private Long dataSetId;
	private String title;
	@CompletionField(maxInputLength = 100, payloads = true, indexAnalyzer = "standard", searchAnalyzer = "standard")
	private Completion suggest;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getDataSetId() {
		return dataSetId;
	}

	public void setDataSetId(Long dataSetId) {
		this.dataSetId = dataSetId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Completion getSuggest() {
		return suggest;
	}

	public void setSuggest(Completion suggest) {
		this.suggest = suggest;
	}

	public static IndexQuery toIndexQuery(FilmLocationDocument document) {
		IndexQuery indexQuery = new IndexQuery();
		indexQuery.setId(document.getId());
		indexQuery.setObject(document);
		return indexQuery;
	}

	public static FilmLocationDocument from(FilmLocation  filmLocation) {
		FilmLocationDocument document = new FilmLocationDocument();
		document.setId(String.valueOf(filmLocation.getId()));
		document.setDataSetId(filmLocation.getFilmLocationDataSet().getId());
		document.setTitle(filmLocation.getTitle());
		Completion suggest = new Completion(new String[]{filmLocation.getTitle()});
		document.setSuggest(suggest);
		return document;
	}
}
