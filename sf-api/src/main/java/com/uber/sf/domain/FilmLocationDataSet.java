package com.uber.sf.domain;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.persistence.AttributeConverter;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class FilmLocationDataSet {
	public static class StateConverter implements AttributeConverter<State, String> {
		@Override
		public String convertToDatabaseColumn(State attribute) {
			return attribute.getValue();
		}

		@Override
		public State convertToEntityAttribute(String dbData) {
			return Optional
					.ofNullable(Arrays.stream(State.values()).filter(s -> s.getValue().equals(dbData)).findFirst())
					.map(Optional::get)
					.orElseThrow(() -> new IllegalStateException("found invalid dataset state: " + dbData));
		}
	}

	public static enum State {
		ACTIVE("A"), OLD("O"), STAGE("S");
		private String value;

		private State(String value) {
			this.value = value;
		}

		public String getValue() {
			return value;
		}
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModified;
	@Convert(converter = StateConverter.class)
	private State state;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "filmLocationDataSet", orphanRemoval = true, cascade = CascadeType.ALL)
	private List<FilmLocation> filmLocations;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public List<FilmLocation> getFilmLocations() {
		return filmLocations;
	}

	public void setFilmLocations(List<FilmLocation> filmLocations) {
		this.filmLocations = filmLocations;
	}

	@Override
	public String toString() {
		return "FilmLocationDataSetInfo [id=" + id + ", lastModified=" + lastModified + ", state=" + state + "]";
	}
}