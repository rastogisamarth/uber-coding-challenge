package com.uber.sf.apidoc;

import javax.servlet.http.HttpServletRequest;

import org.jsondoc.core.annotation.Api;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.view.RedirectView;

@Controller
@CrossOrigin
@Api(description = "shows api documentations", name = "api doc")
public class ApiDocController {
	
	@RequestMapping("/api")
	public RedirectView get(HttpServletRequest request) {
		String url = "http://" + request.getServerName();
		if(request.getLocalPort() != 80) {
			url += ":" + request.getServerPort();
		}
		url = url + "/jsondoc";
		return new RedirectView("/jsondoc-ui.html?url=" + url);
	}
}
