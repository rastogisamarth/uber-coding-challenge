package com.uber.sf.service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uber.sf.domain.FilmLocation;
import com.uber.sf.domain.GeoLocation;
import com.uber.sf.repository.FilmLocationDocumentRepository;
import com.uber.sf.repository.FilmLocationRepository;
import com.uber.sf.task.FilmLocationDataUpdateTask;
import com.uber.sf.web.FilmLocationVO;

@Service
public class FilmLocationServiceImpl implements FilmLocationService {
	private static final Logger logger = LoggerFactory.getLogger(FilmLocationServiceImpl.class);
	@Inject
	private FilmLocationRepository filmLocationRepository;
	@Inject
	private FilmLocationDocumentRepository filmTitleDocumentRepository;
	@Inject
	private GeoLocationService geoLocationService;
	@Inject
	private FilmLocationDataUpdateTask filmLocationDataUpdateTask;
	
	@Override
	public List<FilmLocationVO> getAllFilmLocation(long dataSetId) {
		List<FilmLocation> filmLocations = filmLocationRepository.findByTitleNotNullAndGeoLocation_IdNotNullAndFilmLocationDataSet_Id(dataSetId);
		logger.debug("total film locations with valid title and geolocation for dataset {}: #{}", dataSetId, filmLocations.size());
		return filmLocations.stream().map(FilmLocationVO::from).collect(Collectors.toList());
	}
	
	@Override
	public List<FilmLocationVO> findFilmLocationByTitle(long dataSetId, String title) {
		logger.debug("searching film locations by {} for dataset {}", title, dataSetId);
		List<FilmLocation> filmLocations = filmLocationRepository.findByFilmLocationDataSet_IdAndLocationsNotNullAndAndTitleIgnoreCase(dataSetId, title);
		logger.debug("found {} {} for dataset {}", filmLocations.size(), title, dataSetId);
		List<FilmLocation> validFilmLocations = filmLocations.stream()
				.filter(fl -> Objects.nonNull(fl.getGeoLocation()))
				.filter(fl -> !geoLocationService.isInvalidLocation(fl.getGeoLocation()))
				.collect(Collectors.toList());
		List<FilmLocation> filmLocationsWithoutGeoLocation = filmLocations.stream()
				.filter(fl -> Objects.isNull(fl.getGeoLocation()))
				.collect(Collectors.toList());
		
		Map<String, GeoLocation> locToGeo = geoLocationService.getGeoLocationAsyncFor(
				filmLocationsWithoutGeoLocation.stream().map(FilmLocation::getLocations).collect(Collectors.toList()));
		filmLocationsWithoutGeoLocation.forEach(fl -> {
			if (locToGeo.containsKey(fl.getLocations())) {
				fl.setGeoLocation(locToGeo.get(fl.getLocations()));
			}
		});

		final List<FilmLocation> remainingFilmLocationsWithGeo = filmLocationsWithoutGeoLocation.stream()
				.filter(fl -> Objects.nonNull(fl.getGeoLocation()))
				.collect(Collectors.toList());
		logger.debug("remaining film locations {}", remainingFilmLocationsWithGeo.size());
		List<FilmLocation> remainingValidFilmLocations = remainingFilmLocationsWithGeo.stream()
				.filter(fl -> !geoLocationService.isInvalidLocation(fl.getGeoLocation()))
				.collect(Collectors.toList());
		validFilmLocations.addAll(remainingValidFilmLocations);
		logger.debug("remaining valid film locations {}", remainingValidFilmLocations.size());
		logger.debug("total valid film locations {}", validFilmLocations.size());
		logger.debug("update #{} filmlocations async", remainingFilmLocationsWithGeo.size());
		filmLocationDataUpdateTask.updateFilmLocationForGeoLocation(remainingFilmLocationsWithGeo);
		return validFilmLocations.stream().map(FilmLocationVO::from).collect(Collectors.toList());
	}

	@Override
	public List<String> suggestFilmTitleStartingWith(long dataSetId, String text) {
		logger.debug("fetching suggestions by {} for dataset {}", text, dataSetId);
		return filmTitleDocumentRepository.findByFilmTitleStartingWith(dataSetId, text);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void deleteByDataSetIds(List<Long> dataSetIds) {
		filmLocationRepository.deleteByFilmLocationDataSet_IdIn(dataSetIds);
	}
	
	@Override
	@Transactional(readOnly = false)	
	public void update(FilmLocation filmLocation) {
		filmLocationRepository.save(filmLocation);
	}
}
