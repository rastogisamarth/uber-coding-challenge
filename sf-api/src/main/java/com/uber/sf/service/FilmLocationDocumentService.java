package com.uber.sf.service;

import java.util.List;

import com.uber.sf.domain.FilmLocationDocument;

public interface FilmLocationDocumentService {
	public void deleteByDataSetIds(List<Long> dataSetIds);
	public void store(List<FilmLocationDocument> filmLocationDocuments);
}
