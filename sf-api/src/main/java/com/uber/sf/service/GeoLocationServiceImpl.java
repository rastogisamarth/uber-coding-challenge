package com.uber.sf.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.Cache;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.PendingResult;
import com.google.maps.PendingResult.Callback;
import com.google.maps.errors.NotFoundException;
import com.google.maps.errors.ZeroResultsException;
import com.google.maps.model.ComponentFilter;
import com.google.maps.model.GeocodingResult;
import com.uber.sf.domain.GeoLocation;
import com.uber.sf.repository.GeoLocationRepository;
import com.uber.sf.util.Constant;

@Service
public class GeoLocationServiceImpl implements GeoLocationService {
	private static final Logger logger = LoggerFactory.getLogger(GeoLocationServiceImpl.class);
	
	@Value("${geocoding.api.country}")
	private String country;
	@Value("${geocoding.api.locality}")
	private String locality;
	@Value("${geocoding.api.key}")
	private String geocodingApiKey;
	@Value("#{cacheManager.getCache('" + Constant.META_CACHE + "')}")
	private Cache metaCache;
	@Inject
	private GeoLocationRepository geoLocationRepository;

	private GeoLocation invalidLocation;
	private GeoApiContext geoApiContext;

	@PostConstruct
	private void initialize() {
		geoApiContext = new GeoApiContext().setApiKey(geocodingApiKey);
		invalidLocation = geoLocationRepository.findByLocation(Constant.INVALID_LOCATION);
		if (Objects.isNull(invalidLocation)) {
			invalidLocation = new GeoLocation();
			invalidLocation.setLocation(Constant.INVALID_LOCATION);
			invalidLocation = geoLocationRepository.saveAndFlush(invalidLocation);
			metaCache.putIfAbsent(invalidLocation.getLocation(), invalidLocation);
		}
	}
	
	@Override
	public Map<String, GeoLocation> getGeoLocationAsyncFor(List<String> locations) {
		final Map<String, GeoLocation> locToGeo = new HashMap<>();
		locations.forEach(l -> {
			GeoLocation geoLocation = getGeoLocationFor(l);
			if (Objects.nonNull(geoLocation)) {
				locToGeo.put(l, geoLocation);
			}
		});
		if (locToGeo.size() < locations.size()) {
			final int remainingLocation = locations.size() - locToGeo.size();
			logger.debug("get geolocation from api for #{} locations", remainingLocation);
			final CountDownLatch countDownLatch = new CountDownLatch(remainingLocation);
			locations.forEach(l -> {
				if (!locToGeo.containsKey(l)) {
					getGeocodingFor(l, new Consumer<GeoLocation>() {
						@Override
						public void accept(GeoLocation geoLocation) {
							if (Objects.nonNull(geoLocation)) {
								logger.debug("received geocode for location: {}", geoLocation.getLocation());
								locToGeo.putIfAbsent(l, geoLocation);								
							} else {
								logger.error("could not find geocode for location: {}", l);
							}
							countDownLatch.countDown();
						}
					});
				}
			});
			try {
				countDownLatch.await(10, TimeUnit.SECONDS);
			} catch (InterruptedException e) {} // We don't bother much
		}
		return locToGeo;
	}
	
	@Override
	public GeoLocation getGeoLocationFor(String location) {
		if (Objects.isNull(location) || "".equals(location)) {
			return null;
		}
		logger.debug("get geolocation from cache...");
		GeoLocation geoLocation = metaCache.get(location, GeoLocation.class);
		if (Objects.isNull(geoLocation)) {
			logger.debug("get geolocation from db...");
			geoLocation = geoLocationRepository.findByLocation(location);
			if (Objects.nonNull(geoLocation)) {
				metaCache.putIfAbsent(location, geoLocation);
			}
		}
		logger.debug("found geolocation {} for {}", geoLocation, location);
		return geoLocation;
	}

	private void getGeocodingFor(final String location, final Consumer<GeoLocation> consumer) {
		PendingResult.Callback<GeocodingResult[]> callback = new Callback<GeocodingResult[]>() {
			@Override
			public void onFailure(Throwable e) {
				if (e.getClass().isAssignableFrom(ZeroResultsException.class) ||
						e.getClass().isAssignableFrom(NotFoundException.class)) {
					consumer.accept(invalidLocation);
				} else {
					logger.error("error occurred while accessing geocoding api.", e);
					consumer.accept(null);
				}
			}
	
			@Override
			public void onResult(GeocodingResult[] result) {
				if (Objects.nonNull(result) && result.length > 0) {
					GeoLocation geoLocation = new GeoLocation();
					geoLocation.setLocation(location);
					geoLocation.setLatitude(result[0].geometry.location.lat);
					geoLocation.setLongitude(result[0].geometry.location.lng);
					consumer.accept(geoLocation);
				} else {
					consumer.accept(invalidLocation);
				}
			}
		};
		GeocodingApi.newRequest(geoApiContext)
				.address(location)
				.components(ComponentFilter.country(country), ComponentFilter.locality(locality))
				.setCallback(callback);
		logger.debug("requested geocode async for location: {}", location);
	}

	@Override
	@Transactional(readOnly = false)
	public GeoLocation storeIfNotExist(GeoLocation geoLocation) {
		GeoLocation storedGeoLocation = geoLocationRepository.findByLocation(geoLocation.getLocation());
		if (Objects.isNull(storedGeoLocation)) {
			storedGeoLocation = geoLocationRepository.save(geoLocation);
		}
		return storedGeoLocation;
	}

	@Override
	public boolean isInvalidLocation(GeoLocation geoLocation) {
		return invalidLocation.equals(geoLocation);
	}
}
