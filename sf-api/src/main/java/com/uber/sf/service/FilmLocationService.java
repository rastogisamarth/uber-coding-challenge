package com.uber.sf.service;
import java.util.List;

import com.uber.sf.domain.FilmLocation;
import com.uber.sf.web.FilmLocationVO;

public interface FilmLocationService {

	public List<FilmLocationVO> getAllFilmLocation(long dataSetId);
	public List<String> suggestFilmTitleStartingWith(long dataSetId, String text);
	public List<FilmLocationVO> findFilmLocationByTitle(long dataSetId, String title);
	public void deleteByDataSetIds(List<Long> dataSetIds);
	public void update(FilmLocation filmLocation);
}
