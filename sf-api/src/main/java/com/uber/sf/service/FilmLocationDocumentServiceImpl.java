package com.uber.sf.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.uber.sf.domain.FilmLocationDocument;
import com.uber.sf.repository.FilmLocationDocumentRepository;

@Service
public class FilmLocationDocumentServiceImpl implements FilmLocationDocumentService {
	@Inject
	private FilmLocationDocumentRepository filmLocationDocumentRepository;

	@Override
	public void deleteByDataSetIds(List<Long> dataSetIds) {
		filmLocationDocumentRepository.deleteByFilmLocationDataSetIdIn(dataSetIds);
	}

	@Override
	public void store(List<FilmLocationDocument> filmLocationDocuments) {
		filmLocationDocumentRepository.save(filmLocationDocuments);
	}
}
