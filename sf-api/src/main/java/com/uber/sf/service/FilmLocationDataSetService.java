package com.uber.sf.service;
import java.util.List;
import java.util.Optional;

import com.uber.sf.domain.FilmLocationDataSet;
import com.uber.sf.domain.FilmLocationDataSet.State;

public interface FilmLocationDataSetService {

	public Optional<Long> getActiveDataSetId();

	public List<FilmLocationDataSet> getByState(State state);
	
	public FilmLocationDataSet store(FilmLocationDataSet dataSet);
	
	public void delete(List<FilmLocationDataSet> dataSets);
	
	public void updateActiveDataSet(FilmLocationDataSet currentActive, FilmLocationDataSet newActive);

}