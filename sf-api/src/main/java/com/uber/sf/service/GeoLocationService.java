package com.uber.sf.service;

import java.util.List;
import java.util.Map;

import com.uber.sf.domain.GeoLocation;

public interface GeoLocationService {

	public Map<String, GeoLocation> getGeoLocationAsyncFor(List<String> location);
	public GeoLocation getGeoLocationFor(String location);
	public GeoLocation storeIfNotExist(GeoLocation geoLocation);
	public boolean isInvalidLocation(GeoLocation geoLocation);

}
