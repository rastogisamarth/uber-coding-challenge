package com.uber.sf.service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uber.sf.domain.FilmLocationDataSet;
import com.uber.sf.domain.FilmLocationDataSet.State;
import com.uber.sf.repository.FilmLocationDataSetRepository;
import com.uber.sf.util.Constant;

@Service
@Transactional(readOnly = true)
public class FilmLocationDataSetServiceImpl implements FilmLocationDataSetService {
	private static final Logger logger = LoggerFactory.getLogger(FilmLocationDataSetServiceImpl.class);
	public static final String ACTIVE_DATASET_KEY = Constant.ACTIVE_DATASET_KEY;
	@Inject
	private FilmLocationDataSetRepository filmLocationDataSetRepository;

	@Override
	@Cacheable(cacheNames = Constant.META_CACHE, unless = "#result.isPresent() == false", key = "#root.target.ACTIVE_DATASET_KEY")
	public Optional<Long> getActiveDataSetId() {
		logger.debug("fetching active data set id from db");
		Optional<Long> dataSetId = Optional.of(filmLocationDataSetRepository.findByState(State.ACTIVE))
				.filter(l -> !l.isEmpty()).map(f -> f.get(0).getId());
		dataSetId.ifPresent(id -> logger.debug("active data set id = {}", id));
		return dataSetId;
	}

	@Override
	public List<FilmLocationDataSet> getByState(State state) {
		return filmLocationDataSetRepository.findByState(state);
	}

	@Override
	@Transactional(readOnly = false)
	public FilmLocationDataSet store(FilmLocationDataSet dataSet) {
		return filmLocationDataSetRepository.save(dataSet);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void delete(List<FilmLocationDataSet> dataSets) {
		filmLocationDataSetRepository.delete(dataSets);
	}

	@Override
	@Transactional(readOnly = false)
	public void updateActiveDataSet(FilmLocationDataSet currentActive, FilmLocationDataSet newActive) {
		if (Objects.nonNull(currentActive)) {
			currentActive.setState(State.OLD);
			filmLocationDataSetRepository.save(currentActive);
		}
		newActive.setState(State.ACTIVE);
		filmLocationDataSetRepository.save(newActive);
	}
}
