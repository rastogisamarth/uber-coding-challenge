package com.uber.sf.util;

public class Constant {
	// cache
	public static final String USER_CACHE = "user-cache";
	public static final String META_CACHE = "meta-cache";
	public static final String ACTIVE_DATASET_KEY = "activeFilmLocationDataSetCacheKey";
	// elastic search
	public static final String FILM_LOCATION = "film-location";
	public static final String DATASET_ID = "dataSetId";
	//api
	public static final String SLASH_SUGGEST = "/suggest";
	public static final String SLASH_ALL = "/all";
	public static final String SLASH_SEARCH = "/search";
	public static final String SLASH_API_VERSION = "/v1";
	
	public static final String STATUS_OK = "OK";
	public static final String STATUS_ERROR = "ERROR";
	public static final String STATUS_NO_DATA = "NO_DATA";
	
	
	public static final String SUGGEST = "suggest";
	
	
	// cache header
	public static final int _7_DAYS = 7 * 24 * 60 * 60;
	// others
	public static final String INVALID_LOCATION = "invalid_location";
	public static final String UTF_8 = "UTF-8";
}
