package com.uber.sf.web;

import static com.jayway.restassured.RestAssured.given;
import static com.jayway.restassured.RestAssured.when;
import static com.uber.sf.util.Constant.SLASH_ALL;
import static com.uber.sf.util.Constant.SLASH_API_VERSION;
import static com.uber.sf.util.Constant.SLASH_SEARCH;
import static com.uber.sf.util.Constant.SLASH_SUGGEST;
import static com.uber.sf.util.Constant.STATUS_OK;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;
import com.uber.sf.Application;
import com.uber.sf.domain.FilmLocation;
import com.uber.sf.domain.FilmLocationDataSet;
import com.uber.sf.domain.FilmLocationDataSet.State;
import com.uber.sf.domain.FilmLocationDocument;
import com.uber.sf.domain.GeoLocation;
import com.uber.sf.repository.FilmLocationDataSetRepository;
import com.uber.sf.repository.FilmLocationDocumentRepository;
import com.uber.sf.service.FilmLocationService;
import com.uber.sf.service.GeoLocationService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@WebIntegrationTest(randomPort = true)
public class FilmLocationControllerIntegrationTest {
	private static final String STATUS_FIELD = "status";
	private static final String DATA_FIELD = "data";
	@Value("${local.server.port}")
	private int serverPort;
	@Inject
	private FilmLocationDataSetRepository dataSetRepo;
	@Inject
	private FilmLocationService filmLocationService;
	@Inject
	private GeoLocationService geoLocationService;
	@Inject
	private FilmLocationDocumentRepository indexRepo;


	@Before
	public void setUp() {
		FilmLocationDataSet dataSet = new FilmLocationDataSet();
		dataSet.setId(1L);
		dataSet.setLastModified(new Date());
		dataSet.setState(State.ACTIVE);
		
		FilmLocation filmLocation1 = new FilmLocation();
		filmLocation1.setTitle("180");
		filmLocation1.setLocations("Randall Musuem");
		filmLocation1.setFilmLocationDataSet(dataSet);
		FilmLocation filmLocation2 = new FilmLocation();
		filmLocation2.setTitle("About a Boy");
		filmLocation2.setLocations("Crissy Field");
		filmLocation2.setFilmLocationDataSet(dataSet);

		FilmLocation filmLocation3 = new FilmLocation();
		filmLocation3.setTitle("Quitters");
		filmLocation3.setLocations("Geary from 22nd Ave to Arguello");
		filmLocation3.setFilmLocationDataSet(dataSet);
		dataSet.setFilmLocations(Arrays.asList(filmLocation1, filmLocation2, filmLocation3));
		indexRepo.save(dataSetRepo.save(dataSet).getFilmLocations().stream().map(FilmLocationDocument::from).collect(Collectors.toList()));
		filmLocation3 = dataSet.getFilmLocations().get(2);
		
		GeoLocation geoLocation = new GeoLocation();
		geoLocation.setLocation("Geary from 22nd Ave to Arguello");
		geoLocation.setLatitude(1.0);
		geoLocation.setLongitude(1.0);
		geoLocation = geoLocationService.storeIfNotExist(geoLocation);
		filmLocation3.setGeoLocation(geoLocation);		
		filmLocationService.update(filmLocation3);
		RestAssured.port = serverPort;
		RestAssured.baseURI = "http://localhost";
	}

	@Test
	public void testGetAllWhenFilmLocationsPresentShouldReturnData() {
		ExtractableResponse<Response> extractableResponse = when().get(SLASH_API_VERSION + SLASH_ALL).then().statusCode(HttpStatus.SC_OK).body(STATUS_FIELD, is(STATUS_OK)).extract();
		List<Map<String, Object>> actualFilmLocations = extractableResponse.body().jsonPath().getList(DATA_FIELD);
		assertThat(actualFilmLocations.size(), equalTo(1));
		assertThat(actualFilmLocations.get(0).get("title"), equalTo("Quitters"));		
	}

	@Test
	public void testSuggestWhenFilmLocationsPresentShouldReturnSuggestion() {
		ExtractableResponse<Response> extractableResponse = given().param("q", "18").when().get(SLASH_API_VERSION + SLASH_SUGGEST).then().statusCode(HttpStatus.SC_OK).body(STATUS_FIELD, is(STATUS_OK)).extract();
		List<String> actualTitles = extractableResponse.body().jsonPath().getList(DATA_FIELD);
		assertThat(actualTitles, hasItem("180"));
		assertThat(actualTitles.size(), equalTo(1));
	}

	@Test
	public void testSuggestWhenFilmLocationsNotPresentShouldReturnNoSuggestion() {
		ExtractableResponse<Response> extractableResponse = given().param("q", "xy").when().get(SLASH_API_VERSION + SLASH_SUGGEST).then().statusCode(HttpStatus.SC_OK).body(STATUS_FIELD, is(STATUS_OK)).extract();
		List<String> actualTitles = extractableResponse.body().jsonPath().getList(DATA_FIELD);
		assertThat(actualTitles.size(), equalTo(0));
	}

	@Test
	public void testSearchWhenFilmLocationsPresentShouldReturnData() {
		ExtractableResponse<Response> extractableResponse = given().param("q", "180").when().get(SLASH_API_VERSION + SLASH_SEARCH).then().statusCode(HttpStatus.SC_OK).body(STATUS_FIELD, is(STATUS_OK)).extract();
		List<Map<String, Object>> actualFilmLocations = extractableResponse.body().jsonPath().getList(DATA_FIELD);
		assertThat(actualFilmLocations.size(), equalTo(1));
		assertThat(actualFilmLocations.get(0).get("title"), equalTo("180"));
		assertThat(actualFilmLocations.get(0).get("locations"), equalTo("Randall Musuem"));
		assertThat(actualFilmLocations.get(0).get("latitude"), notNullValue());		
		assertThat(actualFilmLocations.get(0).get("longitude"), notNullValue());		
	}

	@Test
	public void testSearchWhenNoFilmLocationsPresentShouldReturnNoData() {
		ExtractableResponse<Response> extractableResponse = given().param("q", "xy").when().get(SLASH_API_VERSION + SLASH_SEARCH).then().statusCode(HttpStatus.SC_OK).body(STATUS_FIELD, is(STATUS_OK)).extract();
		List<Map<String, Object>> actualFilmLocations = extractableResponse.body().jsonPath().getList(DATA_FIELD);
		assertThat(actualFilmLocations.size(), equalTo(0));
	}
}
