package com.uber.sf.config;

import static org.elasticsearch.node.NodeBuilder.nodeBuilder;

import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.node.Node;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

@Configuration
public class TestConfig {

	@Bean
	public ElasticsearchTemplate elasticSearchTemplate() {
		ImmutableSettings.Builder elasticsearchSettings = ImmutableSettings.settingsBuilder()
				.put("http.enabled", "false").put("path.data", "target/elasticsearch-data");
		Node node = nodeBuilder().local(true).settings(elasticsearchSettings.build()).node();
		Client client = node.client();
		ElasticsearchTemplate template = new ElasticsearchTemplate(client);
		return template;
	}
}
